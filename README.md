# Roman Numerals

[![pipeline status](https://gitlab.com/spaceshell_j/roman-numerals/badges/master/pipeline.svg)](https://gitlab.com/spaceshell_j/roman-numerals/commits/master)

> A JavaScript module to convert a number into Roman numeral format

## Table of Contents
1. [Requirements](#requirements)
1. [Install](#Install)
1. [Usage](#Usage)
1. [Testing](#testing)
1. [License](#License)

## Install

```bash
$ git clone https://gitlab.com/spaceshell_j/roman-numerals.git
```

## Usage

```JavaScript
import RomanNumerals from './roman-numerals/main'

let numeralString = RomanNumerals(223) //numeralString = 'CCXXIII'
```

## Usage

Testing is performed using `Jest` and will test the function to validate both the expected output as well as the out of range failures.

```bash
$ npm run test
```

## License

[MIT](http://vjpr.mit-license.org)
